package main

import (
	"gitlab.com/alameya/somelib/modules"
	"gitlab.com/alameya/somelib/modules/bar"
	"gitlab.com/alameya/somelib/modules/foo"
)

func main() {

	do(foo.Foo)
	do(bar.Bar)
}

func do(someFunc modules.SomeFunc)  {
		someFunc()
}